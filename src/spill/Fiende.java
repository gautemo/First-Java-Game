/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Gaute
 */
public class Fiende {
    int x;
    int y;
    int str = 25;
    boolean opp;
    boolean venstre;
    private int treg =0;
    boolean slow = false;
    
    public Fiende(Brikke spiller){
        double random1 = Math.random();
        double random2 = Math.random();
        boolean nære = true;
        while(nære){
            x = 500 - (int)(Math.random()*500);
            y = 500 - (int)(Math.random()*500);        
            if(Math.abs(x-spiller.x) > 100 || Math.abs(y-spiller.y) >100){
                nære = false;
            }
        }
        if(random1 >0.50){
            opp = true;
        }else{
            opp = false;
        }
        if(random2 >0.50){
            venstre = true;
        }else{
            venstre = false;
        }        
    }
    
    public void tegn(Graphics tegneflate){
        tegneflate.setColor(Color.black);
        tegneflate.fillOval(x, y, str, str);
        tegneflate.setColor(Color.red);
        tegneflate.fillOval(x+5,y+4,str/3,str/3);
        tegneflate.fillOval(x+15,y+4,str/3,str/3);
    }
    
    public void beveg(){
        if(x==0){
            venstre = false;
        }else if(x>=500-str){
            venstre = true;
        }
        if(y==0){
            opp = false;
        }else if(y>=500-str){
            opp = true;
        } 
        if(!slow || (slow && treg%5==0)){
            if(opp){
                y--;
            }else{
                y++;
            }
            if(venstre){
                x--;
            }else{
                x++;
            }
        }
        treg++;
    }
    
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getStr(){
        return str;
    }
    public void slowPå(){
        slow = true;
    }
    public void slowAv(){
        slow = false;
    }
}
