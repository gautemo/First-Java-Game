/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Gaute
 */
public class Brikke {
    int x = 250;
    int y = 250;
    int str = 30;
    boolean udød = false;
    boolean slow = false;
    
    public void tegn(Graphics tegneflate,boolean død){
        tegneflate.setColor(Color.yellow);
        tegneflate.fillOval(x, y, str, str);  //hode
        if(!død){
            tegneflate.setColor(Color.blue);
            tegneflate.fillOval(x+7,y+5,7,7); //øyer
            tegneflate.fillOval(x+17,y+5,7,7);
            tegneflate.setColor(Color.black);
            tegneflate.fillOval(x+9,y+7,4,4); //pupiller
            tegneflate.fillOval(x+19,y+7,4,4);
        }else{
            tegneflate.setColor(Color.black);
            tegneflate.drawLine(x+8, y+4, x+13, y+10);
            tegneflate.drawLine(x+13, y+4, x+8, y+10);
            tegneflate.drawLine(x+18, y+4, x+23, y+10);
            tegneflate.drawLine(x+23, y+4, x+18, y+10);            
        }
        tegneflate.setColor(Color.GRAY);
        if(udød){
            tegneflate.setColor(Color.blue);
        } else if(slow){
            tegneflate.setColor(Color.GREEN);
        }
        if(udød && slow){
            tegneflate.setColor(Color.MAGENTA);
        }
        tegneflate.drawOval(x,y,str,str); //hodesirkel
        tegneflate.setColor(Color.white);
        int[] munnX = {x+5,x+25,x+20,x+15,x+10};
        int[] munnY = {y+20,y+20,y+23,y+25,y+23};     
        tegneflate.fillPolygon(munnX, munnY, munnX.length); //munn
    }
    
    public void venstre(){
        if(x>0)x=x-2;
    }
    public void høyre(){
        if(x<500-str)x=x+2;
    }
    public void opp(){
        if(y>0)y=y-2;
    }
    public void ned(){
        if(y<500-str)y=y+2;
    }
    
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getStr(){
        return str;
    }
    
    public void udød(){
        udød = true;
    }
    public void ferdigUdød(){
        udød = false;
    }
    public void slow(){
        slow = true;
    }
    public void ferdigSlow(){
        slow = false;
    }
}
