/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.io.*;

/**
 *
 * @author Gaute
 */
public class Serialiser {
    
    public void lagre(int tid,int rekord) throws IOException{
        if(tid>rekord){
            FileOutputStream utstrøm = new FileOutputStream("rekord.ser");
            ObjectOutputStream ut = new ObjectOutputStream(utstrøm);
            ut.writeObject(tid);
            ut.close();
        }
    }
    
    public int getRek(){
        try{
            FileInputStream innstrøm = new FileInputStream("rekord.ser");
            ObjectInputStream inn = new ObjectInputStream(innstrøm);
            int rek = (int)inn.readObject();
            return rek;
        }catch(Exception e){
            return 0;
        }
    }
}
