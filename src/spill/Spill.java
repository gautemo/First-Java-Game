/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.io.IOException;

/**
 *
 * @author Gaute
 */
public class Spill {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, IOException{
        Serialiser lagre = new Serialiser();
        while(true){
            int rekord = lagre.getRek(); 
            Vindu vindu = new Vindu(rekord);
            vindu.nyFI();
            vindu.nyFI();
            boolean fiende = false;
            boolean udød = false;
            boolean slow = false;
            while(!vindu.sjekk()){
                while(vindu.pause()){
                }
                if(vindu.tid() != 0 && vindu.tid()%5==0 && !udød){
                    vindu.nyUdød();
                    udød = true;
                }
                if(vindu.tid()%5!=0){
                    udød = false;
                }
                if(vindu.tid() != 0 && vindu.tid()%5==0 && !slow){
                    vindu.nySlow();
                    slow = true;
                }
                if(vindu.tid()%5!=0){
                    slow = false;
                }            
                vindu.beveg();
                vindu.oppdater();
                if(vindu.tid() != 0 && vindu.tid()%3==0 && !fiende){
                    vindu.nyFI();
                    fiende = true;
                }
                if(vindu.tid()%3!=0){
                    fiende = false;
                }
                Thread.sleep(15);
            }
            vindu.død();
            vindu.oppdater();
            lagre.lagre(vindu.tid(),rekord);
            vindu.enter();
            while(!vindu.restart()){
            }
            vindu.setVisible(false);
        }
    }
}
