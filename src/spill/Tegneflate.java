/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import javax.swing.JLabel;

/**
 *
 * @author Gaute
 */
public class Tegneflate extends JLabel{
    Brikke spiller;
    ArrayList<Fiende> fi = new ArrayList<Fiende>();
    Udødlig udødlig;
    GregorianCalendar tid;
    int rekord;
    boolean død = false;
    boolean udød = false;
    long udødTid;
    int tidenIgjenU =0;
    int tidenIgjenS =0;
    Slow slow;
    long slowTid;
    long pause =0;
    long pausestart;
    boolean startPause = false;
    
    public Tegneflate(Brikke spiller, int rek){
        this.spiller=spiller;
        this.tid = new GregorianCalendar();
        this.rekord = rek;
    }
    
    public void paintComponent(Graphics tegneflate){
        tegneflate.setColor(Color.LIGHT_GRAY);
        tegneflate.fillRect(0, 0, 500, 500);
        tegneflate.setColor(Color.black);
        tegneflate.drawRect(0, 0, 500, 500);
        tegneflate.setFont(new Font("SansSerif",Font.ITALIC,15));        
        tegneflate.drawString(""+tid(),470,20);
        tegneflate.drawString(""+rekord,20,20);
        tegneflate.setColor(Color.blue);
        tegneflate.fillRect(350, 40, tidenIgjenU/35, 10);
        tegneflate.setColor(Color.GREEN);
        tegneflate.fillRect(350, 60, tidenIgjenS/35, 10);
        spiller.tegn(tegneflate,død);
        if(udødlig !=null) udødlig.tegn(tegneflate);
        if(slow !=null) slow.tegn(tegneflate);
        for(int i=0; i<fi.size();i++){
            fi.get(i).tegn(tegneflate);
        }
    }
    
    public void nyUdød(){
        double sjanse = Math.random();
        if(sjanse >0.80){
            udødlig = new Udødlig();
        }else{
            udødlig = null;
        }
        
    }
    public void nySlow(){
        double sjanse = Math.random();
        if(sjanse >0.80){
            slow = new Slow();
        }else{
            slow = null;
        }
        
    }    
    
    public void død(){
        død = true;
    }
    
    public int tid(){
        GregorianCalendar nå = new GregorianCalendar();
        long millisekunder = nå.getTimeInMillis() - tid.getTimeInMillis() -pause;
        int sekunder = (int)millisekunder/1000;
        return sekunder;
    }
    public long tidMillis(){
        GregorianCalendar nå = new GregorianCalendar();
        long millisekunder = nå.getTimeInMillis() - tid.getTimeInMillis() -pause;
        return millisekunder;
    }
    
    public void pause(){
        if(!startPause){
            pausestart = tidMillis();
        }
        startPause = true;
        pause += tidMillis()-pausestart;
    }
    public void unPause(){
        startPause = false;
    }
    
    private void sjekkPower(){
        if(udødlig != null && spiller.getX()+spiller.str> udødlig.x && spiller.getX() <udødlig.x+udødlig.str && spiller.getY()+spiller.str > udødlig.y && spiller.getY()< udødlig.y+udødlig.str){
            udød = true;
            spiller.udød();
            udødTid=tidMillis();
            udødlig = null;
        }
        if(slow != null && spiller.getX()+spiller.str> slow.x && spiller.getX() <slow.x+slow.str && spiller.getY()+spiller.str > slow.y && spiller.getY()< slow.y+slow.str){
            for(Fiende fiende : fi){
                fiende.slowPå();
            }
            spiller.slow();
            slowTid=tidMillis();
            slow = null;
        }        
    }
    private void ferdigPower(){
        if(tidMillis()-udødTid >= 5000){
            udød = false;
            spiller.ferdigUdød();
            tidenIgjenU =0;
        }else if(udød){
            tidenIgjenU = (int)(5000-(tidMillis()-udødTid));
        }
        if(tidMillis()-slowTid >= 5000){
            for(Fiende fiende : fi){
                fiende.slowAv();
            }            
            spiller.ferdigSlow();
            tidenIgjenS =0;
        }else if(fi.get(0).slow){
            for(Fiende fiende : fi){
                fiende.slowPå();
            }
            tidenIgjenS = (int)(5000-(tidMillis() -slowTid));
        }        
    }
    
    public boolean sjekk(){
        sjekkPower();
        ferdigPower();
        if(udød) return false;
        for(Fiende fiende : fi){
            if(spiller.getX()+spiller.str> fiende.getX() && spiller.getX() <fiende.getX()+fiende.str && spiller.getY()+spiller.str > fiende.getY() && spiller.getY()< fiende.getY()+fiende.str){
                return true;
            }
        }
        return false;
    }
    
    public void nyFI(){
        Fiende ny = new Fiende(spiller);
        fi.add(ny);
    }
    
    public void bevegFI(){
        for(Fiende fiende : fi){
            fiende.beveg();
        }
    }
}
