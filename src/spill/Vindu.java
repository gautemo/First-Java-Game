/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spill;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

/**
 *
 * @author Gaute
 */
public class Vindu extends JFrame{
    Brikke spiller = new Brikke();
    boolean venstre = false;
    boolean høyre = false;
    boolean opp = false;
    boolean ned = false;
    Tegneflate tegning;
    int rekord;
    private boolean restart = false;
    boolean pause = false;
    
    public Vindu(int rek){
        this.rekord = rek;
        setTitle("Spill");
        setSize(517,539);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.tegning = new Tegneflate(spiller, rek);
        TastLytter lytter = new TastLytter();
        this.addKeyListener(lytter);
        add(tegning);
        setVisible(true);
    }
    
    public void nyFI(){
        tegning.nyFI();
    }
    
    public void beveg(){
        if(venstre){
            spiller.venstre();
        }
        if(høyre){
            spiller.høyre();
        }
        if(opp){
            spiller.opp();
        }
        if(ned){
            spiller.ned();
        }
        tegning.bevegFI();
    }
    
    public void oppdater(){
        tegning.repaint();
    }
    
    public boolean pause(){
        if(pause){
            tegning.pause();
            return pause;
        }
        tegning.unPause();
        return pause;
    }
    
    public int tid(){
        return tegning.tid();
    }
    
    public boolean sjekk(){
        return tegning.sjekk();
    }
    public void nyUdød(){
        tegning.nyUdød();
    }
    public void død(){
        tegning.død();
    }
    public void nySlow(){
        tegning.nySlow();
    }
    public boolean restart(){
        if(restart){
            setVisible(false);
            spiller = new Brikke();
            tegning = new Tegneflate(spiller,rekord);
            this.add(tegning);
            tegning.repaint();
            setVisible(true);
            return restart;
        }
        return restart;
    }
    public void sjekkRekord(int rekorden){
        rekord = rekorden;
    }
    public void enter(){
        restart = false;
    }
        
    private class TastLytter implements KeyListener{
        
        public void keyReleased(KeyEvent e){
            int keyCode = e.getKeyCode();
            switch(keyCode){
                case KeyEvent.VK_LEFT:
                    venstre = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    høyre = false;
                    break;
                case KeyEvent.VK_UP:
                    opp = false;
                    break;
                case KeyEvent.VK_DOWN:
                    ned = false;
                    break;
                case KeyEvent.VK_ENTER:
                    restart = true;
                    break;
                /*case KeyEvent.VK_P:
                    if(pause){
                        pause = false;
                    }else{
                        pause = true;
                    }
                    break;*/                    
                    
            }
        }
        public void keyPressed(KeyEvent e){
            int keyCode = e.getKeyCode();
            switch(keyCode){
                case KeyEvent.VK_LEFT:
                    venstre = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    høyre = true;
                    break;
                case KeyEvent.VK_UP:
                    opp = true;
                    break;
                case KeyEvent.VK_DOWN:
                    ned = true;
                    break;
                case KeyEvent.VK_ENTER:
                    restart = true;
                    break;
                case KeyEvent.VK_P:
                    if(pause){
                        pause = false;
                    }else{
                        pause = true;
                    }
                    break;
            }            
        }
        public void keyTyped(KeyEvent e){
            int keyCode = e.getKeyCode();
            switch(keyCode){
                case KeyEvent.VK_LEFT:
                    spiller.venstre();
                    break;
                case KeyEvent.VK_RIGHT:
                    spiller.høyre();
                    break;
                case KeyEvent.VK_UP:
                    spiller.opp();
                    break;
                case KeyEvent.VK_DOWN:
                    spiller.ned();
                    break;
                case KeyEvent.VK_ENTER:
                    restart = true;
                    break;
                case KeyEvent.VK_P:
                    if(pause){
                        pause = false;
                    }else{
                        pause = true;
                    }
                    break;                    
            }            
        }        
    }
}
